jQuery(document).ready(function($) {

    $('.sign-form').submit(function () {
        var $form = $(this);
        var $inputs = $form.find('input');
        var has_error = false;
        $.each($inputs, function(index, item) {
            item = $(item);
            if (item.attr('type') == 'submit' || item.attr('type') == 'reset')
                return true;
            if (!item.val()) {
                item.css('border', '2px solid red');
                item.focus();
                setTimeout(function() {
                    item.removeAttr('style');
                }, 1000);
                has_error = true;
                return false;
            }
        });
        if (has_error)
            return false;
        var serializedData = $form.serialize();
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : serializedData,
            dataType : 'json',
            cache : false,
            beforeSend : function () {
                $('#button-submit').attr('disabled', 'disabled');
            },
            success : function (msg) {
                if (msg.error) {
                    $('#button-submit').removeAttr('disabled');
                    alert(msg.msg)
                } else {
                    if ($form.hasClass('signup-form')) {
                        window.location.href = '/signin';
                    } else if ($form.hasClass('signin-form')) {
                        window.location.href = '/';
                    }
                }
            },
            error : function (XMLHttpRequest, textStatus, errorThrown) {
                $('#button-submit').removeAttr('disabled');
                alert('网络出错！');
            }
        });
        return false;
    });


    $('.msg-form').submit(function () {
        var $form = $(this);
        if (!$('#msg').val()) {
            $('#msg').css('border', '2px solid red').focus();
            setTimeout(function() {
                $('#msg').removeAttr('style');
            }, 1000);
            return false;
        }
        var serializedData = $form.serialize();
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : serializedData,
            dataType : 'json',
            cache : false,
            beforeSend : function () {
                $('#button-submit').attr('disabled', 'disabled');
            },
            success : function (msg) {
                if (msg.error) {
                    alert(msg.msg)
                } else {
                    $.ajax({
                        url : '/loadmsg/0/1?with_li=0',
                        type : 'get',
                        cache : false,
                        success : function (data) {
                            $('.new-msg').html(data).slideDown().attr('id', 'msg-' + msg.id).removeClass('new-msg');
                            $('#add-msg').after('<li class="new-msg" style="display:none"></li>');
                        }
                    });
                    $('#msg').val('');
                }
                $('#button-submit').removeAttr('disabled');
            },
            error : function (XMLHttpRequest, textStatus, errorThrown) {
                $('#button-submit').removeAttr('disabled');
                alert('网络出错！');
            }
        });
        return false;
    });

    $('.del-msg').on('click', function () {
        if ($(this).attr('data-delete') == '0') {
            $(this).html('<i class="icon-remove-sign"></i> 删除');
            $(this).css('color', 'red');
            $(this).attr('data-delete', '1');
            $(this).attr('title', '再次点击删除留言');
            setTimeout(function () {
                $('.del-msg').attr('data-delete', '0').removeAttr('style').html('<i class="icon-remove-sign"></i>');
            }, 2000);
            return false;
        }
        var href = $(this).attr('href');
        var msg_id = href.replace('/delmsg/', '');

        $.ajax({
            url : href,
            type : 'get',
            cache : false,
            success : function (msg) {
                if (msg.error) {
                    alert(msg.msg);
                } else {
                    $('#msg-' + msg_id).slideUp();
                    setTimeout(function () {
                        $('#msg-' + msg_id).remove();
                    }, 1000);
                }
            }
        });
        return false;
    });

    $('.reply-msg').on('click', function () {
        var form = $($(this).attr('href'));
        form.slideToggle();
        form.find('textarea').focus();
        return false;
    });

    $('.reply-form').submit(function () {
        var $form = $(this);
        var textarea = $form.find('textarea');
        if (!textarea.val()) {
            textarea.css('border', '2px solid red').focus();
            setTimeout(function() {
                textarea.removeAttr('style');
            }, 1000);
            return false;
        }
        var serializedData = $form.serialize();
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : serializedData,
            dataType : 'json',
            cache : false,
            beforeSend : function () {
                $form.find('input').attr('disabled', 'disabled');
            },
            success : function (msg) {
                if (msg.error) {
                    alert(msg.msg)
                } else {
                    $form.siblings('.action').find('.reply-msg').remove();
                    $form.slideUp();
                    setTimeout(function () {
                        $form.remove();
                    }, 1000);
                }
                $form.find('input').removeAttr('disabled');
            },
            error : function (XMLHttpRequest, textStatus, errorThrown) {
                $form.find('input').removeAttr('disabled');
                alert('网络出错！');
            }
        });
        return false;
        return false;
    });
});