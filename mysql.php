<?php
namespace guestbook;

class mysql {

    private $_config;
    /** @var object 数据库连接句柄 */
    private $_dbh = null;
    /** @var bool 是否已连接数据库 */
    private $_is_connected = FALSE;

    public function __construct($config) {
        $this->_config = $config;
    }

    public function __destruct() {
        if ($this->isConnected()) {
            $this->close();
        }
    }
    
    public function connect() {
        if(!$this->_config['host'] || !$this->_config['dbname'] || !$this->_config['username'] || !$this->_config['password']) {
            exit('数据库配置信息错误!');
        }
        $this->_dbh = mysql_connect($this->_config['host'], $this->_config['username'], $this->_config['password']);
        if($this->_dbh) {
            $this->_is_connected = TRUE;
            mysql_select_db($this->_config['dbname'], $this->_dbh);
            mysql_query('SET NAMES "utf8"');
        } else {
            die('Could not connect:' . mysql_error());
        }
        return TRUE;
    }
    
    public function isConnected() {
        return $this->_is_connected;
    }
    
    public function close() {
        mysql_close($this->_dbh);
        $this->_dbh = null;
        $this->_is_connected = FALSE;
        return TRUE;
    }
    
    public function &native() {
        return $this->_dbh;
    }
    
    public function errorCode() {
        if($this->isConnected()) {
            return mysql_errno($this->_dbh);
        }
        return 0;
    }
    
    public function errorInfo() {
        if($this->isConnected()) {
            return mysql_error($this->_dbh);
        }
        return '';
    }
    
    public function lastInsertId() {
        if($this->isConnected()) {
            return mysql_insert_id($this->_dbh);
        }
        return FALSE;
    }
    
    public function query($sql) {
        if($this->isConnected()) {
            $argnum = func_num_args();
            if ($argnum > 1) {
                $args = func_get_args();
                array_shift($args);
                // real escape each item in $args
                array_walk($args, function(&$item, $key) {
                    $item = mysql_real_escape_string($item);
                });
                $sql = vsprintf($sql, $args);
            }
            $sth = mysql_query($sql,$this->_dbh);
            if($sth) {
                $rs = array();
                while(($row = mysql_fetch_object($sth)) !== FALSE) {
                    $rs[] = $row;
                }
                mysql_free_result($sth);
                return $rs;
            } else {
                die('Query Error:' . mysql_error($this->_dbh));
            }
        }
        return array();
    }
    
    public function exec($sql) {
        if($this->isConnected()) {
            $argnum = func_num_args();
            if ($argnum > 1) {
                $args = func_get_args();
                array_shift($args);
                // real escape each item in $args
                array_walk($args, function(&$item, $key) {
                    $item = mysql_real_escape_string($item);
                });
                $sql = vsprintf($sql, $args);
            }
            return mysql_query($sql, $this->_dbh);
        }
        return FALSE;
    }
    
    public function quote($str) {
            return mysql_real_escape_string($str);
        return $str;
    }
    
    
}