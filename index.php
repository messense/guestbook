<?php
require 'vendor/autoload.php';
require 'mysql.php';

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

session_start();

$app = new \zf\App();
$app->register('mysql', '\guestbook\mysql', $app->config->mysql);

$app->mysql->connect();

$app->get('/', 'home');
$app->get('/signup', 'signup');
$app->post('/signup', 'api/signup');
$app->get('/signin', 'signin');
$app->post('/signin', 'api/signin');
$app->get('/signout', 'signout');
$app->post('/addmsg', 'api/addmsg');
$app->get('/delmsg/:id', 'api/delmsg');
$app->post('/replymsg/:id', 'api/replymsg');
$app->get('/loadmsg/:start/:count', 'api/loadmsg');

$app->run();