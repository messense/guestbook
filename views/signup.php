<?php require 'header.php'; ?>
<ul class="gb_timeline">
    <li>
        <div class="gb_tmtime">
            <span>注册</span>
        </div>
        <div class="gb_tmicon icon-user"></div>
        <div class="gb_tmlabel">
            <h2>注册新用户</h2>
            <form class="sign-form signup-form" action="/signup" method="post">
                <div>
                    <label for="username">用户名</label>
                    <input id="username" type="text" name="username" placeholder="Username" />
                </div>
                <div>
                    <label for="nickname">昵称</label>
                    <input id="nickname" type="text" name="nickname" placeholder="Nickname" />
                </div>
                <div>
                    <label for="email">电子邮件</label>
                    <input id="email" type="email" name="email" placeholder="Email" />
                </div>
                <div>
                    <label for="password">密码</label>
                    <input id="password" type="password" name="password" placeholder="Password" />
                </div>
                <div>
                    <input id="button-reset" type="reset" value="重置" />
                    <input id="button-submit" type="submit" value="注册用户" />
                </div>
            </form>
        </div>
    </li>
</ul>
<?php require 'footer.php'; ?>