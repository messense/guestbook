<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?= isset($this->page_title) ? $this->page_title : '留言本' ?></title>
        <meta name="description" content="A guestbook" />
        <meta name="keywords" content="guestbook" />
        <meta name="author" content="messense" />
        <link rel="stylesheet" type="text/css" href="static/css/style.css" />
        <link rel="stylesheet" type="text/css" href="static/css/font-awesome.min.css" />
        <!--[if IE 7]>
        <link rel="stylesheet" href="static/css/font-awesome-ie7.min.css" />
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <header class="clearfix">
                <span>Guestbook</span>
                <a href="/" title="留言本"><h1>留言本</h1></a>
                <nav>
                    <?php if (isset($_SESSION['user']) && $_SESSION['user'] == TRUE) { ?>
                    <a class="icon icon-signout" href="/signout" title="登出"></a>
                    <?php } else { ?>
                    <a class="icon icon-signin" href="/signin" title="登录"></a>
                    <a class="icon icon-user" href="/signup" title="注册"></a>
                    <?php } ?>
                </nav>
            </header>
            <div class="main">