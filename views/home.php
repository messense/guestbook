<?php require 'header.php'; ?>
<ul class="gb_timeline">
    <?php if (isset($_SESSION['user']) && $_SESSION['user'] && !$_SESSION['is_admin'] && $_SESSION['user_id']) { ?>
    <li id="add-msg">
        <div class="gb_tmtime"><span>留言</span></div>
        <div class="gb_tmicon icon-edit"></div>
        <div class="gb_tmlabel">
            <h2><i class="icon-plus-sign"></i> 发表新留言</h2>
            <form class="msg-form" action="/addmsg" method="post">
                <div>
                    <textarea id="msg" name="msg" placeholder="Leave a message"></textarea>
                </div>
                <div>
                    <input id="button-reset" type="reset" value="重置" />
                    <input id="button-submit" type="submit" value="提交留言" />
                </div>
            </form>
        </div>
    </li>
    <?php } ?>
    <li class="new-msg" style="display:none">
        
    </li>
    <?php foreach ($this->msgs as $msg) { ?>
    <li id="msg-<?= $msg->id ?>">
        <time class="gb_tmtime" datetime="<?= date("Y-m-d H:i", $msg->created_at) ?>">
            <span><?= date("m/d/Y", $msg->created_at) ?></span>
            <span><?= date("H:i", $msg->created_at) ?></span>
        </time>
        <div class="gb_tmicon <?php if ($msg->reply) echo 'icon-comments'; else echo 'icon-comment'; ?>"></div>
        <div class="gb_tmlabel">
            <?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin']) { ?>
            <div class="action">
                <?php if (!$msg->reply) { ?>
                <a href="#reply-form-<?= $msg->id ?>" class="reply-msg" title="回复"><i class="icon-reply-all"></i></a>
                <?php } ?>
                <a href="/delmsg/<?= $msg->id ?>" class="del-msg" title="删除" data-delete='0'><i class="icon-remove-sign"></i></a>
            </div>
            <?php } ?>
            <h2><i class="icon-user"></i> <?= $msg->nickname ?></h2>
            <p><?= $msg->content ?></p>
            <?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] && !$msg->reply) { ?>
            <form id="reply-form-<?= $msg->id ?>" class="reply-form" action="/replymsg/<?= $msg->id ?>" method="post">
                    <textarea name="reply" placeholder="Write reply"></textarea>
                    <input type="submit" value="回复" />
            </form>
            <?php } ?>
            <?php if ($msg->reply) { ?>
            <div class="msg-reply">
                <p><i class="icon-comment"></i> [管理员回复]: <?= $msg->reply ?></p>
            </div>
            <?php } ?>
        </div>
    </li>
    <?php } ?> 
</ul>
<?php require 'footer.php'; ?>