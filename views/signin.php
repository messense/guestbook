<?php require 'header.php'; ?>
<ul class="gb_timeline">
    <li>
        <div class="gb_tmtime">
            <span>登录</span>
        </div>
        <div class="gb_tmicon icon-user"></div>
        <div class="gb_tmlabel">
            <h2>用户登录</h2>
            <form class="sign-form signin-form" action="/signin" method="post">
                <div>
                    <label for="username">用户名</label>
                    <input id="username" type="text" name="username" placeholder="Username" />
                </div>
                <div>
                    <label for="password">密码</label>
                    <input id="password" type="password" name="password" placeholder="Password" />
                </div>
                <div>
                    <input id="button-reset" type="reset" value="重置" />
                    <input id="button-submit" type="submit" value="登录" />
                </div>
            </form>
        </div>
    </li>
</ul>
<?php require 'footer.php'; ?>