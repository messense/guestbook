<?php
return function() {
    $sql = "SELECT message.*, user.nickname FROM message INNER JOIN user ON message.user_id=user.id ORDER BY message.created_at DESC";
    $msgs = $this->mysql->query($sql);
    $this->render("home", ['msgs' => $msgs]);
};