<?php
return function () {
    $ret = [
        'error' => 0,
        'msg' => ''
    ];
    if (!isset($_SESSION['is_admin']) || !$_SESSION['is_admin']) {
        $ret['error'] = 1;
        $ret['msg'] = '权限不足！';
        $this->send($ret);
        return;
    }
    $id = intval($this->params->id);

    $sql = "SELECT * FROM message WHERE id=%d";
    if (!$this->mysql->query($sql, $id)) {
        $ret['error'] = 1;
        $ret['msg'] = '此留言不存在！';
        $this->send($ret);
        return;
    }

    $sql = "DELETE FROM message WHERE id=%d";
    if ($this->mysql->exec($sql, $id)) {
        $ret['msg'] = '删除留言成功！';
    } else {
        $ret['error'] = 1;
        $ret['msg'] = '删除留言失败！';
    }
    
    $this->send($ret);
};