<?php
return function () {
    $ret = [
        'error' => 0,
        'msg' => ''
    ];
    if (!isset($_SESSION['is_admin']) || !$_SESSION['is_admin']) {
        $ret['error'] = 1;
        $ret['msg'] = '权限不足！';
        $this->send($ret);
        return;
    }
    $id = intval($this->params->id);

    $sql = "SELECT * FROM message WHERE id=%d";
    if (!$this->mysql->query($sql, $id)) {
        $ret['error'] = 1;
        $ret['msg'] = '此留言不存在！';
        $this->send($ret);
        return;
    }

    $content = isset($_POST['reply']) ? $_POST['reply'] : '';
    if (empty($content)) {
        $ret['error'] = 1;
        $ret['msg'] = '留言内容不能为空！';
        $this->send($ret);
        return;
    }

    $content = htmlentities($content);
    $content = str_replace("\n", "<br/>", $content);

    $timestamp = time();
    $sql = "UPDATE message SET reply='%s',updated_at=%d WHERE id=%d";
    if ($this->mysql->exec($sql, $content, $timestamp, $id)) {
        $ret['msg'] = '留言回复成功！';
    } else {
        $ret['error'] = 1;
        $ret['msg'] = '留言回复失败！';
    }

    $this->send($ret);
};