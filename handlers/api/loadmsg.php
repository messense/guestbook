<?php
return function () {
    $html = '';
    $tpl = '<time class="gb_tmtime" datetime="%s">
            <span>%s</span>
            <span>%s</span>
        </time>
        <div class="gb_tmicon icon-comment"></div>
        <div class="gb_tmlabel">
            <h2><i class="icon-user"></i> %s</h2>
            <p>%s</p>
        </div>';

    $start = $this->params->start;
    $count = $this->params->count;
    $with_li = isset($_GET['with_li']) ? intval($_GET['with_li']) : 1;

    $sql = "SELECT message.*, user.nickname FROM message INNER JOIN user ON message.user_id=user.id ORDER BY message.created_at DESC LIMIT ${start},${count}";
    $msgs = $this->mysql->query($sql);
    foreach ($msgs as $msg) {
        $item = sprintf($tpl, date("Y-m-d H:i", $msg->created_at), date("m/d/Y", $msg->created_at), date("H:i", $msg->created_at), $msg->nickname, $msg->content);
        if ($with_li > 0)
            $item = '<li id="msg-'. $msg->id .'">'. $item .'</li>';
        $html .= $item;
    }
    
    $this->send($html);
};