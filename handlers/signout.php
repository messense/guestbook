<?php
return function() {
    $_SESSION['user'] = FALSE;
    $_SESSION['user_id'] = 0;
    $_SESSION['is_admin'] = FALSE;
    unset($_SESSION['user']);
    unset($_SESSION['user_id']);
    unset($_SESSION['is_admin']);
    header('Location: /');
    exit();
};